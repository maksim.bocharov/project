const getBigestNumber = (...nums) => {
    console.log(nums);
    if(nums.length < 2){
        throw new Error('Not enough arguments')
    } else if(nums.length > 10){
        throw new Error('Too many arguments')
    }  
    nums.forEach(num => {
        if(typeof num !== 'number'){
            // throw new Error('Wrong argument type')
            return false
        }
    });
    
    return nums.reduce((prev, next) => {
        if(prev < next){
            return next;
        }
        return prev
    }, 0)
}

// console.log(getBigestNumber(1,2,6,8,22,1,9))
module.exports = getBigestNumber