let AmountBatteries = +prompt('Enter amount of batteries', '');
let PerDefective = +prompt('Enter a percentage of defective batteries', '');

if(( PerDefective == 'number' && typeof(AmountBatteries) == 'number') || (PerDefective >= 0 && PerDefective <= 100) &&  AmountBatteries >= 0) {
   let defective = (AmountBatteries * PerDefective / 100).toFixed(2);
   let working = (AmountBatteries - defective).toFixed(2);

    alert(`                                                           
    
    Amount of batteries: ${AmountBatteries}
    Defective rate: ${PerDefective}%
    Amount of defective batteries: ${defective}
    Amount of working batteries: ${working}
    `);
    
} else {
    alert('Invalid input data');  
}

