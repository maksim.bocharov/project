const root = document.getElementById('root');
let books = JSON.parse(localStorage.getItem('data'));

layOut();

function layOut() {
    const bookStoreWrapper = document.createElement('div'),
        list = document.createElement('div'),
        description = document.createElement('div'),
        addBtn = document.createElement('button');

    list.classList.add('list');
    description.classList.add('description');
    bookStoreWrapper.classList.add('wrapper');
    bookStoreWrapper.append(list, description);

    addBtn.textContent = 'ADD NEW BOOK';

    root.append(bookStoreWrapper, addBtn);

    addBtn.classList.add('add-new');
    addBtn.addEventListener('click', () => createForm(addItem, addBtn));

    generateBooks(books, list);
}

function generateBooks(data, bookList) {
    bookList.classList.add('list');

    data.forEach(book => {
        const div = document.createElement('div'),
            title = document.createElement('div'),
            btn = document.createElement('button');

        div.classList.add('list-item');

        title.textContent = book.title;
        title.insertAdjacentHTML('beforeend', `<span> by ${book.author}</span>`);
        title.addEventListener('click', () => createDescription(book.id));
        title.classList.add('title');

        div.append(title);
        div.append(btn);
        div.id = book.id;

        btn.textContent = 'EDIT';
        btn.classList = 'btn-edit';
        btn.dataset.for = book.id;
        btn.addEventListener('click', e => createForm(editItem, e.target));

        bookList.append(div);
    })
}

function createForm(initiator, item) {
    if (document.querySelector('.form')) {
        document.querySelector('.form').remove();
    }
    const form = document.createElement('form');
    form.classList.add('form');
    form.insertAdjacentHTML('afterbegin', `
       <label>
       Book name: <input name="title" required type="text">
       </label>
       <label>
       Author: <input name="author" required type="text">
       </label>
       <label>
       Image: <input name="image" required type="text">
       </label>
       <label>
       Plot: <input name="plot" required type="text">
       </label>
       <div class="btn-group">
       <button>SAVE CHANGES</button>
       <a href="#" class="discard">DISCARD CHANGES</a>
       </div>
    `);
    root.append(form);

    form.addEventListener('submit', e => {
        e.preventDefault();

        const formData = new FormData(form),
            dataObj = Object.fromEntries(formData.entries());

        initiator(item, dataObj, form);
    });

    document.querySelector('.discard').addEventListener('click', () => {
        if (confirm('Do you want to discard all changes?')) {
            window.history.back();
        }
    });

    root.append(form);
}

function createDescription(number) {
    window.location = `./index.html?id=${number}#preview`;
    const description = document.querySelector('.description');

    description.innerHTML = '';
    books.forEach(({title, author, img, plot, id}) => {
        if (id === number) {
            description.insertAdjacentHTML('afterbegin', `
            <h2>${title}</h2>
            <div>Author: ${author}</div>
            <img src="${img}" alt="logo">
            <div>${plot}</div>
            `);
        }
    });
}

function editItem(item, {title, author, image, plot}, form) {
    const id = item.dataset.for;

    books.forEach(item => {
        if (item.id === +id) {
            item.title = title;
            item.author = author;
            item.image = image;
            item.plot = plot;

            recreateLayout(form, document.querySelector('.add-new'));
        }

        localStorage.setItem('data', JSON.stringify(books));
    });
}

function addItem(item, {title, author, image, plot}, form) {
    books.push({
        title,
        author,
        image,
        plot,
        img: books.length
    });

    localStorage.setItem('data', JSON.stringify(books));
    recreateLayout(form, item);
}

function recreateLayout(...itemsToRemove) {
    document.querySelector('.wrapper').remove();
    itemsToRemove.forEach(item => {
        item.remove();
    });

    layOut();
}