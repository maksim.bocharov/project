const data = [
  {
    'folder': true,
    'title': 'Grow',
    'children': [
      {
        'title': 'logo.png'
      },
      {
        'folder': true,
        'title': 'English',
        'children': [
          {
            'title': 'Present_Perfect.txt'
          }
        ]
      }
    ]
  },
  {
    'folder': true,
    'title': 'Soft',
    'children': [
      {
        'folder': true,
        'title': 'NVIDIA',
        'children': null
      },
      {
        'title': 'nvm-setup.exe'
      },
      {
        'title': 'node.exe'
      }
    ]
  },
  {
    'folder': true,
    'title': 'Doc',
    'children': [
      {
        'title': 'project_info.txt'
      }
    ]
  },
  {
    'title': 'credentials.txt'
  }
];


const rootNode = document.getElementById('root');

rootNode.classList.add('container');

treeInit(rootNode);
createFileStructure(data, document.querySelector('.root_tree'));
contextMenu();

function treeInit(rootNode) {
  const ul = document.createElement('ul');

  ul.addEventListener('click', e => {
      let clicked = e.target;

      if (clicked.parentElement.nodeName === 'LI') {
          clicked = clicked.parentElement;
      }

      if (clicked && clicked.nodeName === 'LI' && clicked.lastChild.nodeName === 'UL') {

          const child = clicked.lastChild,
              icon = clicked.querySelector('.material-icons');

          if (child.classList.toggle('d-n')) {
              icon.textContent = 'folder';
          } else {
              icon.textContent = 'folder_open';
          }

      }
  });
  ul.classList.add('root_tree');
  rootNode.append(ul);
}

function createFileStructure(data, initialParent) {
  data.forEach(item => {
      const li = document.createElement('li');

      li.innerHTML = `
          <span class="title">${item.title}</span>`;
      initialParent.append(li);
      
      if (item.folder) {
          const ul = document.createElement('ul');

          li.innerHTML = `<span class="title">${item.title}</span>`;
          ul.classList.add('d-n');
          li.insertAdjacentHTML('afterbegin', '<span class="material-icons folder">folder</span>');
          li.append(ul);

          if (item.children) {
              createFileStructure(item.children, ul);
          } else {
              const emptyMessage = document.createElement('li');

              emptyMessage.textContent = 'The folder is empty';
              emptyMessage.style.listStyleType = 'none';

              ul.append(emptyMessage);
          }
      } else {
          li.insertAdjacentHTML('afterbegin', '<span class="material-icons file">insert_drive_file</span>');
    }
  });
}

function contextMenu() {
  const contextMenu = document.createElement('div'),
    todos = [
          {
              title: 'Rename',
              id: 'rename',
              foo: renameItem
          },
          {
              title: 'Delete item',
              id: 'delete',
              foo: deleteItem
          }
      ];

  contextMenu.classList.add('context-menu_wrapper');
  rootNode.append(contextMenu);
  todos.forEach(item => {
      const contextMenuItem = document.createElement('div');

      contextMenuItem.textContent = item.title;
      contextMenuItem.id = item.id;
      contextMenuItem.classList.add('context-menu_item');
      contextMenu.append(contextMenuItem);
  })

  window.addEventListener('click', () => {
      document.querySelector('.context-menu_wrapper').style.display = '';
  })

  window.addEventListener('contextmenu', e => {
      e.preventDefault();

      let selected = e.target;

      if (selected && selected.parentElement.nodeName === 'LI') {
        selected = e.target.parentElement;
        }

        if (selected && selected.nodeName === 'LI') {
            contextMenu.style.cssText = `
            display: block;
            left: ${e.pageX}px; 
            top: ${e.pageY}px; 
            `
            ;

            contextMenu.addEventListener('click', e => {
                if (e.target && e.target.matches('.context-menu_item')) {
                    todos.forEach(({id, foo}) => {
                        if (id === e.target.id) {
                            foo(selected);
                        }
                    });
                }
            }, {once: true});
        }
    });
}

function deleteItem(item) {
  if (item.parentElement.children.length === 1) {
      item.innerHTML = 'The folder is empty';
  } else {
      item.remove();
  }
}

function renameItem(item) {
    const title = item.querySelector('.title'),
        input = document.createElement('input'),
        previousTitle = title.textContent;

    title.innerHTML = '';
    title.append(input);
    input.value = previousTitle;
    input.focus();

    input.addEventListener('blur', () => {
        if (!input.value) {
            title.innerHTML = previousTitle;
        } else {
            title.innerHTML = input.value;
        }

        input.remove();
    });
}