/* START TASK 1: Your code goes here */
function colorYellow(){
    const tdAll = document.querySelectorAll('td');
    const trAll = document.querySelectorAll('tr');
    trAll.forEach(tr => {
        tdAll.forEach(td => {
            td.addEventListener('click',() => {
                console.log(tr)
                if(tr.getAttribute('style')){
                    return;
                } else {
                    td.style.backgroundColor = 'yellow'
                }
            })
        })
    })
}

function colorBlue(){
    const tr = document.querySelectorAll('tr');
    tr.forEach(td_f => {
        let firstCell = td_f.firstElementChild;
        firstCell.addEventListener('click',() => {
            let currentRow = firstCell.parentElement
            currentRow.style.backgroundColor = 'blue';
        })
    })
}

function colorGreen(){
    const special = document.querySelector('.special');
    special.addEventListener('click', () => {
        let allTd = document.querySelectorAll('td');
        allTd.forEach(td => {
            if(td.getAttribute('style')){
                return
            }else {
                special.style.backgroundColor = 'green'
                td.style.backgroundColor = 'green'
            }
        })
    })
    
}
colorYellow()
colorBlue()
colorGreen()
/* END TASK 1 */

/* START TASK 2: Your code goes here */
const message = document.querySelector('.task2__message'),
    input = document.querySelector('input'),
    btn = document.querySelector('button');


message.style.cssText = `
    height: 100px;
    width: 350px;
    border: 1px black solid;
    
`
input.style.cssText = `
    width: 250px;
    height: 45px;
    padding: 0 15px;
`

btn.style.cssText = `
    width: 50px;
    margin-left:15px;

`

function inputNumber(){
    input.addEventListener('input', () => {
        const regExp = /\+380\d{9}/gm;
        if(input.value.match(regExp)){   
            message.innerHTML = `
            Type nomber follows format
            +38**********
            `
            message.style.cssText = `
                height: 100px;
                width: 350px;
                border: 1px black solid;
                background-color: #32CD32;
                padding: 10px;
                color: #fff;
                font-size: 20px;
                transition: 1s all;
            `
            input.style.border = '1px green solid'
            btn.disabled = false;
            
        } else {
            message.innerHTML = `
            Type nomber does not follow format
            +38**********
            `
            message.style.cssText = `
                height: 100px;
                width: 350px;
                border: 1px black solid;
                background-color: #B22222;
                padding: 10px;
                color: #fff;
                font-size: 20px;
                transition: 1s all;
            `
            input.style.border = '1px red solid'
            btn.disabled = true;
        }
    })
    btn.addEventListener('click', () => {
        if(btn.disabled === false){
            message.innerHTML = `
                Data was successfully sent!
            `
            input.value = ''
        }
    })
}


inputNumber()
/* END TASK 2 */

/* START TASK 3: Your code goes here */
const court = document.querySelector('.court'),
      ball = document.querySelector('.ball'),
      wrapper = document.querySelector('.task3__wrapper'),
      team1 = document.createElement('div'),
      team2 = document.createElement('div');

team1.classList.add('team1');
team2.classList.add('team2');
wrapper.append(team1);
wrapper.append(team2);

let scoreTeam2 = document.querySelector('.team1'),
    scoreTeam1 = document.querySelector('.team2');

let countScoreTeam2 = 0;
function score2(){
    scoreTeam2.addEventListener('click', (e) => {
        ball.style.cssText = `
            position: absolute;
            left: ${e.pageX - 18}px;
            top: ${e.pageY - 18}px;
        `  
        
        if (countScoreTeam2 === null) {
            countScoreTeam2 = 0;
            }
            countScoreTeam2++;

            counterTeam2.innerHTML = `
            Score: ${countScoreTeam2}
            `
    })
}

let countScoreTeam1 = 0;
function score1(){
    scoreTeam1.addEventListener('click', (e) => {
        ball.style.cssText = `
            position: absolute;
            left: ${e.pageX - 18}px;
            top: ${e.pageY - 18}px;
        `  
        if (countScoreTeam1 === null) {
            countScoreTeam1 = 0;
            }
            countScoreTeam1++;

            counterTeam1.innerHTML = `
            Score: ${countScoreTeam1}
        `
    })
}

court.addEventListener('click', (e) => {
        ball.style.cssText = `
        position: absolute;
        left: ${e.pageX - 18}px;
        top: ${e.pageY - 18}px;
    `  
})

const counterTeam1 = document.querySelector('.score-team1');
const counterTeam2 = document.querySelector('.score-team2');
let e1 = new Event('scored1');
scoreTeam1.addEventListener('scored1', score1());
scoreTeam1.dispatchEvent(e1);

let e2 = new Event('socred2');
scoreTeam2.addEventListener('scored2', score2());
scoreTeam1.dispatchEvent(e2);
/* END TASK 3 */
