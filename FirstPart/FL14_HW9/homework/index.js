function convert(a, b, c, d) {
    let array = [a, b, c, d];
    for(let i = 0; i < array.length; i++){
        if(typeof(array[i]) === 'string'){
            array[i] = +array[i];
        } else if (typeof(array[i]) === 'number'){
            array[i] = String(array[i]);
        }
    }
    return array;
}

function executeforEach(arr, func) {
    for(let i = 0; i < arr.length; i++){
        func(arr[i]);
    }
    return;
}

function mapArray(arr, func){
    let mapedArray = [];
    executeforEach(arr, function(elem) {
        mapedArray.push(func(+elem));
    });

    return mapedArray;
}

function filterArray(arr, func){
    let filteredArray = [];
    executeforEach(arr, function(elem){
        if(func(elem)){
            filteredArray.push(func(elem))
        }
    });
    return filteredArray;
}

function getValuePosition(arr, elem){
    for(let i = 0; i < arr.length; i++) {
        if(arr[i] === elem){
            return i - 1;
        }
    }
    return false;
}

function flipOver(str) {
    let newString = "";
 
    for (let i = str.length - 1; i >= 0; i--) { 
        newString += str[i];
    }
    
    return newString;
}
 
function makeListFromRange(range) {
    const list = [];
    for (let i = range[0]; i <= range[1]; i++) {
        list.push(i);
    }
    return list;
}

function getArrayOfKeys(arr, key){
    let new_arr = [];
    executeforEach(arr, function(el){
        if(el[key]){
            new_arr.push(el[key])
        }
    });
    return new_arr;
}

function getTotalWeight(basket){
    let weight = 0;
    executeforEach(basket, item => {
        weight += item.weight;
    });
    return weight;
}

function getPastDate(day){
    let date = new Date(2020, 10, 17), new_date = new Date();
    new_date = date.getDate() - day;
    return new_date;
}

function formatDate(date){
    let year = date.getFullYear(), 
        month = date.getMonth() + 1, 
        day = date.getDate(),
        hours = date.getHours(),
        minutes = date.getMinutes(),
        seconds = date.getSeconds();

        if(day < 10){
            day = '0' + day;
        }
        if(month < 10){
            month = '0' + month;
        }
        if(hours < 10){
            hours = '0' + hours;
        }
        if(minutes < 10){
            minutes = '0' + minutes;
        }
        if(seconds < 10){
            seconds = '0' + seconds;
        }
    return `${year}/${month}/${day} ${hours}:${minutes}:${seconds}`; 
}