function isEquals(a, b){
    return a === b;
}

function numberToString(num){
    return String(num);
}

function storeNames(...str){
    return str;
}

function getDivision(a,b){
    if(b >= a){
        return b/a;
    } else {
        return a/b;
    }
}

function negativeCount(array){
    let counter = 0;
    for(let i = 0; i< array.length; i++){
        if(array[i] < 0){
            counter++;
        }
    }
    return counter;
}

function letterCount(word, letter){
    let counter = 0;
    for(let i = 0; i < word.length; i++){
        if(word[i] === letter){
            counter++;
        }
    }
    return counter;
}

function countPoints (results){
    results = results.map(item => item.split(':').map(el => +el));
    let sum = 0;
    results.forEach((current) => {
        if(current[0] > current[1]){
            sum = sum + 3;
        } else if (current[0] === current[1]) {
            sum++;
        }
    });
    return sum;
}