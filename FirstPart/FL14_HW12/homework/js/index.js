function visitLink(path) {
	let count = localStorage.getItem(path);
	if (count === null) {
	count = 0;
	}
	count++;
	localStorage.setItem(path, count);
}

function viewResults() {
const btn = document.querySelector('.btn'),
	new_div = document.createElement('ul');

	new_div.innerHTML = `
	<li>Page1 clicked ${localStorage.getItem('Page1')} times</li>
	<li>Page2 clicked ${localStorage.getItem('Page2')} times</li>
	<li>Page3 clicked ${localStorage.getItem('Page3')} times</li>
	`

	btn.insertAdjacentElement('afterend', new_div);

}
