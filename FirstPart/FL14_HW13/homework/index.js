const getAge = (year, month, day) => {
    let myBirthday = new Date(year, month - 1, day),
        curr_date = new Date();
    
    if(myBirthday){
        if(curr_date.getMonth() > myBirthday.getMonth()){
                return curr_date.getFullYear() - myBirthday.getFullYear();
        } else if(curr_date.getMonth() < myBirthday.getMonth()){
            return curr_date.getFullYear() - myBirthday.getFullYear() - 1;
        } else if(curr_date.getMonth() === myBirthday.getMonth()) {
            if(curr_date.getDate() >= myBirthday.getDate()){
                return curr_date.getFullYear() - myBirthday.getFullYear();
            } else {
                return curr_date.getFullYear() - myBirthday.getFullYear() - 1;
            }
        }   
    }
}

const getWeekDay = (date) => {
    const days = [
        'Sunday',
        'Monday',
        'Tuesday',
        'Wednesday',
        'Thursday',
        'Friday',
        'Saturday' 
      ];
      return days[date.getDay()];
}

const getProgrammersDay = year => {
    const date = new Date(year, 0, 256),
        month = date.toLocaleString('en', {
            month: 'short'
        });

    return `${date.getDate()} ${month}, ${year} (${getWeekDay(date)})`;
}

const howFarIs = weekDay => {
    const date = new Date(),
        correctWeekDay = weekDay.toLowerCase(),
        weekDayForAnswer = correctWeekDay[0].toUpperCase() + correctWeekDay.slice(1);

    for (let i = 0; i < 7; i++) {
        if (getWeekDay(date).toLowerCase() === correctWeekDay) {
            if (i) {
                return `It's ${i} day(s) left till ${weekDayForAnswer}.`
            }

            return `Hey, today is ${weekDayForAnswer} =)`;
        }

        date.setDate(date.getDate() + 1);
    }

    return 'Invalid input';
}

const isValidIdentifier = (str) => {
    const regExp = new RegExp(/^\d|[^a-zA-Z\d/$_]|^$|\s/gi).test(str)
    if(!regExp){
        return true
    } else {
        return false
    }
}

const capitalize = (str) => {
    return str.replace(/(^|\s)\S/g, (letter) => {
        return letter.toUpperCase()
    });
}

const isValidAudioFile = (str) => {
    const regExp = new RegExp(/^\w+(\.mp3|\.flac|\.alac|\.aac)$/).test(str);
    return regExp;
}

const getHexadecimalColors = (str) => {
    return str.match(/#(([A-Fa-f\d]{6})(?=\s|$)|([A-Fa-f\d]{3})(?=\s|$))/gim)
}                      

const isValidPassword = (str) => {
    const regExp = new RegExp(/(?=.*[\d])(?=.*[a-z])(?=.*[A-Z])[\da-zA-Z!@#$.%^&*]{8,}/gm).test(str);
    return regExp;
}

const addThousandsSeparators = (str) => {
    return str.toString().replace(/(?=(\d{3})+(?!\d))/g, ',');
}