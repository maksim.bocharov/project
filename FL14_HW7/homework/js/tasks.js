async function findMax(...nums){
    return await Math.max(...nums);
}

async function copiedArray(array){
    let copied = await array.slice();
    
    return copied;
}

function addUniqueId(obj){
    let id = Symbol('id')
    let main = {
        name: 'Alex'
    }
    let copy = Object.assign({}, obj, main, id);

    copy.id = id;
    return copy;
}

let oldObj = {
    name: 'Someone',
    details: {
        id: 1,
        age: 11,
        university: 'UNI'
    } 
}

// let {details: {university}, details:user} = oldObj
// console.log(user)
// let newObj = Object.assign({}, {university}, {user})
// console.log(newObj)

async function findUniqueElements(array){
    let res = []
    await array.forEach(item => {
        if(!res.includes(item)){
            res.push(item)
        }
    });
    return res;
}

function phoneNumber(number){
    number = number.split('');
    for(let i = number.length - 5; i>= 0; i--){
        number[i] = '*'
    }
    number = number.join('')
    return number
}

function add(a = 0, b = 0){
    if(!a || !b){
        throw new Error('Missing property')
    } else {
        return a + b;
    }
}

function sortAlphabet(){
    let response = fetch('https://jsonplaceholder.typicode.com/users');
    response.then(response => response.json())
    .then(json => json.forEach(item => {
        item = item.name;
        console.log(item.replace(/\n/, 'g'))
        
    }))
}

