const root = document.querySelector('.root')
const url = 'https://jsonplaceholder.typicode.com/users'
function initAllUsers(){
    fetch(url)
    .then(response => response.json())
    .then(json => {
	json.forEach(item => {
		let h3 = document.createElement('h3');
		h3.setAttribute('id', item.id)
		h3.innerHTML = `
			<li style="list-style-type:none">${item.name}, ${item.id}</li>
			<button id=${item.id}>Edit</button>
			<button id=${item.id}>Delete</button>
    	`
		root.append(h3)
		});
    })
}

function deleteUser(url, id){
  fetch(`${url}/${id}`, {
    method: 'DELETE'
  }).then(response => alert(`${response} ///// User with id ${id} deleted!`));
}

function editUser(){
	const btns = document.querySelectorAll('button');
	const h3 = document.querySelectorAll('h3')
	h3.forEach(h => {
		btns.forEach(btn => {
			btn.addEventListener('click', (e) => {
				e.preventDefault();
				if(e.target.id === h.id){
					let editFiled = prompt('Enter editing name', '');
					h.innerHTML = `
						${editFiled}
					`
					fetch(`https://jsonplaceholder.typicode.com/users/${h.id}`, {
						method: 'PUT',
						body: JSON.stringify({
							name: editFiled
						})
					}).then(response => response.json())
					.then(json => alert(`User ${json.id} changed`))
				}
			})
		})
	})
}

function showAllUsers(){
  fetch('https://jsonplaceholder.typicode.com/users')
    .then(response => response.json())
    .then(json => console.log(json))
}

function showComments(){
	const h3 = document.querySelectorAll('h3');
	h3.forEach(h => {
		h.addEventListener('click', () => {
			fetch(`https://jsonplaceholder.typicode.com/comments/${h.id}`)
			.then(response => response.json())
			.then(json => {
				let div = document.createElement('div');
				div.innerHTML = `
					${json.body}
				`
				h.append(div);
			})

		})
	})	  
}

