'use strict';
 
class Main {
    constructor() {
        this.val = '';
        this.elements = [];
    }
}
 
class ResourceManager extends Main {
    constructor(val) {
        super();
        this.val = val;
    }
 
    addElement(el) {
        this.elements.push(el);
    }
}
 
class DefaultEmployee extends Main {
    constructor(val) {
        super();
 
        this.val = val;
    }
}
 
fetch('mock.json')
    .then(res => res.json())
    .then(res => tree(res))
    .catch(err => console.error(err));

const wrapper = document.createElement('div'),
      body = document.querySelector('body')
wrapper.style.cssText = `
    margin: 0 auto;
    width: 1320px;
    padding:100px;
`
body.append(wrapper)
function tree(res) {
    const employees = [];

    res.forEach(val => {
        if (val['pool_name']) {
            employees.push(new ResourceManager(val))
        } else {
            employees.push(new DefaultEmployee(val))
        }
    });
    // console.log(employees);
    employees.forEach(item => {
        if (item.val['rm_id']) {
            employees[item.val['rm_id'] - 1].addElement(item);
        } 
    });
    employees.forEach(item => {
        wrapper.append(item.val.name)
    })
 
    const tree = employees[0];
    // wrapper.append(tree)
    console.log(tree);
}