const todos = [
  {
    text: "Buy milk",
    done: false
  },
  {
    text: "Play with dog",
    done: true
  }
];

$(document).ready(function(){
	const $list = $(".list");
	const $input = $("#add-input");
	const $add = $("#add-submit");
	const $item_text = $('.item-text');
	const $remove = $('.item-remove');
	
	$item_text.each(function() {
		$(this).click(function() {
			if(!$(this).hasClass('done')){
				$(this).addClass('done')
			} 
		})
	});

	$remove.each(function(){
		$(this).click(function(){
			$(this).parent().remove()
		})
	});

	(function($){          // $ === jQuery
		$.fn.addTask = function() { 
		  if($input.val()!==''){
			$list.append(`<li class='item'>
				<span class='item-text'>${$input.val()}</span>
				<button class='item-remove'>Remove</button>
			  </li>`);
			todos.push({text: $input.val(), done: false});
		  }
		  $input.val('')
		  event.preventDefault();
		};
	  }(jQuery));
	  
	  $add.click(function(i = 1){
		$(this).addTask();
		
		if(!localStorage.getItem('Todo')){
			localStorage.setItem('Todo', $input.val())
		} else {
			localStorage.setItem(`Todo ${i}`,  $input.val());
			i = i + 1
		}
	  });
	
	$('#search').keyup(function(){
		_this = this;

		$.each($('.item'), function(){
			if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1) {
				$(this).hide();
			} else {
				$(this).show();                
			}
		})
	})
});