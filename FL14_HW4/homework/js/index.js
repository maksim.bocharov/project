class Desk {
    constructor(){
        this._count = 52;
        let hearts = [];
        let spades = [];
        let clubs = [];
        let diamonds = [];
        for (let i = 0; i < 13 ; i++) {
            diamonds[i] = i + 1 + ' Diamonds';
            hearts[i] = i + 1 + ' Hearts';
            clubs[i] = i + 1 + ' Clubs';
            spades[i] = i + 1 + ' Spades';
        }
        
        this.cards = [...hearts, ...spades, ...clubs, ...diamonds];

        for(let i = 0; i < this.cards.length; i++){

            switch(i % 13) {
                case 0: this.cards[i] += ' Ace'
                break;
                case 10: this.cards[i] += ' Jack'
                break
                case 11: this.cards[i] += ' Queen';
                break
                case 12: this.cards[i] += ' King';
                break
                default: break;
            }
            
        }   
    }
    
    shuffle(){
        for (let i = this.cards.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            let temp = this.cards[i];
            this.cards[i] = this.cards[j];
            this.cards[j] = temp;
        }
        
        return this.cards
    }

    draw(n){
        return this.cards.splice(this.cards.length - n, this.cards.length);
    }
}


class Card{
    constructor(suit, rank){
        this.suit = suit;
        this.rank = rank;
        
        switch(suit){
            case 'Hearts': this.suit = suit;
            break;
            case 'Diamonds': this.suit = suit;
            break;
            case 'Clubs': this.suit = suit;
            break;
            case 'Spades': this.suit = suit;
            break;
            default: throw new Error();
        }

        if(rank >= 1 && rank <= 13){
            this.rank = rank;
            switch(rank){
                case 1:
                this.isFaceCard = true;
                break;
                case 11: 
                this.isFaceCard = true
                break;
                case 12:
                this.isFaceCard = true
                break;
                case 13:
                this.isFaceCard = true
                break;
                default: this.rank = {rank: rank}
            }   
            } else {
                throw new Error()
            }
        

    }

    toString(){
        switch(this.rank) {
             case 1: return `Ace of ${this.suit}`
            case 11: return `Jack of ${this.suit}`
            case 12: return `Queen of ${this.suit}`
            case 13: return `King of ${this.suit}`
            default: return `${this.rank} of ${this.suit}`
        }
    }

    compare(cardOne, cardTwo){
        if(cardOne.rank > cardTwo.rank){
            return `C1 > C2`
        } else {
            return `C1 < C2`
        }
    }
}

class Player{
    constructor(name){
        this.name = name;
        this.deck = new Desk();
    }
    play(playerOne, playerTwo){
        this.deck.shuffle();
        
        this.playerOne = this.deck.draw(1);
        this.playerTwo = this.deck.draw(1);
        
        this.playerOne = this.playerOne[0].split('');
        this.playerTwo = this.playerTwo[0].split('');

        this.playerOne = this.playerOne.splice(0, 1);
        this.playerTwo = this.playerTwo.splice(0, 1);
        
        this.playerOne = Number(this.playerOne);
        this.playerTwo = Number(this.playerTwo);
        console.log(`player ${playerOne.name} has ${this.playerOne}`);
        console.log(`palyer ${playerTwo.name} has ${this.playerTwo}`);

        let winsP1 = 0, winsP2 = 0;

        if(this.playerOne === this.playerTwo){
            return `Nobody Wins`;
        }
        if(this.playerOne > this.playerTwo){
            winsP1 = winsP1 + 1;
            return `player ${playerOne.name} win and has ${winsP1} wins`
        } else {
            winsP2 = winsP2 + 1;
            return `player ${playerTwo.name} win and has ${winsP2} wins`
        }
        
    }
}


