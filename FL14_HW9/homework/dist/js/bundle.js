/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/js/palyGame.js":
/*!****************************!*\
  !*** ./src/js/palyGame.js ***!
  \****************************/
/*! namespace exports */
/*! export palyGame [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__.r, __webpack_exports__, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "palyGame": () => /* binding */ palyGame
/* harmony export */ });
 function palyGame(){
    let countMe = localStorage.getItem('My score');
    if(countMe === null){
        countMe = 0;
    }
    let countBot = localStorage.getItem('Bot score');
    if(countBot === null){
        countBot = 0;
    }

    const array = ['Rock', 'Scissors', 'Paper'],
          myElement = document.querySelectorAll('.game-control button'),
          battle = document.querySelector('.battle'),
          win = document.querySelector('.win'),
          lost = document.querySelector('.lost');
    
    myElement.forEach(item => {
        item.addEventListener('click', (e) => {
            const botElement = array[Math.floor(Math.random()*array.length)],
                  br = document.createElement('br');

            if(countMe === 3){
                    battle.append('You are win!');
                    win.style.cssText = `
                        display:block;
                    `
                    return 0;
            } else if(countBot === 3){
                    battle.append('Bot are win :(')
                    lost.style.cssText = `
                        display:block;
                    `

                    return 0;
            }
            if(item.textContent === botElement){
                battle.append(`Nothing happen! Bot also chose ${botElement}`, br);
            }else {   
                if(e.target && e.target.nodeName === 'BUTTON' && countMe < 3 && countBot < 3){
                    switch(item.textContent){
                        case 'Paper':
                            if(botElement === 'Scissors'){
                                countBot = countBot + 1;
                                localStorage.setItem('Bot score', countBot);
                                battle.append(`${botElement} win Paper, You’ve LOST!.`, br)
                            } else {
                                countMe = countMe + 1;
                                localStorage.setItem('My score', countMe);
                                battle.append(`Paper win ${botElement}, You’ve WON!`, br)
                            }
                        break;
                        case 'Rock':
                            if(botElement === 'Paper'){
                                countBot = countBot + 1;
                                localStorage.setItem('Bot score', countBot);
                                battle.append(`${botElement} win Rock, You’ve LOST!.`, br)
                            } else {
                                countMe = countMe + 1;
                                localStorage.setItem('My score', countMe);
                                battle.append(`Rock win ${botElement}, You’ve WON!`, br)
                            }
                        break;
                        case 'Scissors': 
                            if(botElement === 'Rock'){
                                countBot = countBot + 1;
                                localStorage.setItem('Bot score', countBot);
                                battle.append(`${botElement} win Scissors, You’ve LOST!.`, br)
                            } else {
                                countMe = countMe + 1;
                                localStorage.setItem('My score', countMe);
                                battle.append(`Scissors win ${botElement}, You’ve WON!`, br)
                            }
                        break;
                        default: break;
                    }
                }
            }   
        })
    })
}



/***/ }),

/***/ "./src/js/resetBtn.js":
/*!****************************!*\
  !*** ./src/js/resetBtn.js ***!
  \****************************/
/*! namespace exports */
/*! export resetBtn [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__.r, __webpack_exports__, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "resetBtn": () => /* binding */ resetBtn
/* harmony export */ });
function resetBtn(){
    const reset = document.querySelector('.reset'),
          battle = document.querySelector('.battle');

    reset.addEventListener('click', () => {
    localStorage.clear();
    battle.innerHTML = '';
    location.reload()
    })
}

/***/ }),

/***/ "./src/js/script.js":
/*!**************************!*\
  !*** ./src/js/script.js ***!
  \**************************/
/*! namespace exports */
/*! exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__, __webpack_require__.r, __webpack_exports__, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _startGame__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./startGame */ "./src/js/startGame.js");
/* harmony import */ var _palyGame__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./palyGame */ "./src/js/palyGame.js");
/* harmony import */ var _resetBtn__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./resetBtn */ "./src/js/resetBtn.js");




(0,_startGame__WEBPACK_IMPORTED_MODULE_0__.startGame)();
(0,_palyGame__WEBPACK_IMPORTED_MODULE_1__.palyGame)();
(0,_resetBtn__WEBPACK_IMPORTED_MODULE_2__.resetBtn)()




/***/ }),

/***/ "./src/js/startGame.js":
/*!*****************************!*\
  !*** ./src/js/startGame.js ***!
  \*****************************/
/*! namespace exports */
/*! export startGame [provided] [no usage info] [missing usage info prevents renaming] */
/*! other exports [not provided] [no usage info] */
/*! runtime requirements: __webpack_require__.r, __webpack_exports__, __webpack_require__.d, __webpack_require__.* */
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "startGame": () => /* binding */ startGame
/* harmony export */ });
function startGame(){
    const startBtn = document.querySelector('.start'),
          gameControl = document.querySelector('.game-control');
    startBtn.addEventListener('click', ()=> {
        startBtn.parentElement.classList.add('hide');
        gameControl.classList.toggle('show');
    });
}

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		if(__webpack_module_cache__[moduleId]) {
/******/ 			return __webpack_module_cache__[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop)
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	// startup
/******/ 	// Load entry module
/******/ 	__webpack_require__("./src/js/script.js");
/******/ 	// This entry module used 'exports' so it can't be inlined
/******/ })()
;
//# sourceMappingURL=bundle.js.map