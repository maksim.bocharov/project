export function resetBtn(){
    const reset = document.querySelector('.reset'),
          battle = document.querySelector('.battle');

    reset.addEventListener('click', () => {
    localStorage.clear();
    battle.innerHTML = '';
    location.reload()
    })
}