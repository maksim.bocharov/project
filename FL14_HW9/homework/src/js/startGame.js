export function startGame(){
    const startBtn = document.querySelector('.start'),
          gameControl = document.querySelector('.game-control');
    startBtn.addEventListener('click', ()=> {
        startBtn.parentElement.classList.add('hide');
        gameControl.classList.toggle('show');
    });
}