 export function palyGame(){
    let countMe = localStorage.getItem('My score');
    if(countMe === null){
        countMe = 0;
    }
    let countBot = localStorage.getItem('Bot score');
    if(countBot === null){
        countBot = 0;
    }

    const array = ['Rock', 'Scissors', 'Paper'],
          myElement = document.querySelectorAll('.game-control button'),
          battle = document.querySelector('.battle'),
          win = document.querySelector('.win'),
          lost = document.querySelector('.lost');
    
    myElement.forEach(item => {
        item.addEventListener('click', (e) => {
            const botElement = array[Math.floor(Math.random()*array.length)],
                  br = document.createElement('br');

            if(countMe === 3){
                    battle.append('You are win!');
                    win.style.cssText = `
                        display:block;
                    `
                    return 0;
            } else if(countBot === 3){
                    battle.append('Bot are win :(')
                    lost.style.cssText = `
                        display:block;
                    `

                    return 0;
            }
            if(item.textContent === botElement){
                battle.append(`Nothing happen! Bot also chose ${botElement}`, br);
            }else {   
                if(e.target && e.target.nodeName === 'BUTTON' && countMe < 3 && countBot < 3){
                    switch(item.textContent){
                        case 'Paper':
                            if(botElement === 'Scissors'){
                                countBot = countBot + 1;
                                localStorage.setItem('Bot score', countBot);
                                battle.append(`${botElement} win Paper, You’ve LOST!.`, br)
                            } else {
                                countMe = countMe + 1;
                                localStorage.setItem('My score', countMe);
                                battle.append(`Paper win ${botElement}, You’ve WON!`, br)
                            }
                        break;
                        case 'Rock':
                            if(botElement === 'Paper'){
                                countBot = countBot + 1;
                                localStorage.setItem('Bot score', countBot);
                                battle.append(`${botElement} win Rock, You’ve LOST!.`, br)
                            } else {
                                countMe = countMe + 1;
                                localStorage.setItem('My score', countMe);
                                battle.append(`Rock win ${botElement}, You’ve WON!`, br)
                            }
                        break;
                        case 'Scissors': 
                            if(botElement === 'Rock'){
                                countBot = countBot + 1;
                                localStorage.setItem('Bot score', countBot);
                                battle.append(`${botElement} win Scissors, You’ve LOST!.`, br)
                            } else {
                                countMe = countMe + 1;
                                localStorage.setItem('My score', countMe);
                                battle.append(`Scissors win ${botElement}, You’ve WON!`, br)
                            }
                        break;
                        default: break;
                    }
                }
            }   
        })
    })
}

